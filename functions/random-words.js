const _ = require('underscore');
const nlp = require('compromise');
const w2v = require("word2vec-pure-js")
const logToNaturalDatabase = require("../utils/log-to-natural-database")
w2v.load("./word2vec-models/test-text8-vector.bin")

/**
* Get random words form the provided text.
* @returns {array}
*/
module.exports = async (text = "", numberOfWords = 1, uniq = true, excludes = [], wordTypes = [], similarTo = "") => {
  let relatedTerms = _.shuffle(w2v.getSimilarWords(similarTo, numberOfWords));
  logToNaturalDatabase(JSON.stringify(relatedTerms));

  let relatedTermsEnrichedByCompromiseNlp = nlp(relatedTerms.join(" ")).out("terms");

  let randomWords = _.map(relatedTermsEnrichedByCompromiseNlp, (term, index)=> {
    return {word: term.text, termNormal: term.normal};
  });

  return randomWords;
};
